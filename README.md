# Example Image #
![paginator_example.png](https://bitbucket.org/repo/BxaXey/images/1615207405-paginator_example.png)

# README #

This is wrapper class of the Paginator on Django.

### What is this repository for? ###

* Pagination is easier to use with Mket Paginator

### How do I get set up? ###


```
#!python

python setup.py install
```


### Python Code Example ###
```
#!python
from mket_paginator import Paginator

paginator = Paginator(objects, current_page, PER_PAGE, PAGINATION_RANGE)
```
objects (QuerySet) : the objects of a Model.

current_page (int): you might get "request.GET.get('page', 1)"

PER_PAGE (int): the number of objects used in a page

PAGINATION_RANGE (int): the number of index pages






### Template Example ###

```
#!html
<style>
.goog-inline-block {
	position: relative;
	display: -moz-inline-box; /* Ignored by FF3 and later. */
	display: inline-block;
}
* html .goog-inline-block {
	display: inline;
}
*:first-child+ html .goog-inline-block {
	display: inline;
}
.example_page{
    border-radius: 5px;
    font-size: 20px;
    font-weight: bold;
    background-color: #aaa;
    padding-left: 5px;
    padding-right: 5px;
    padding-bottom: 3px;
    padding-top: 3px;
    color: #fff;
}
.example_selected_page{
    background-color: #EF1C67;
}
</style>
<div>
	{%if paginator.has_previous%}
	<a href="?page={{paginator.previous}}">
	<div class="goog-inline-block example_page">
		Previous
	</div> </a>
	<div class="goog-inline-block">
		...
	</div>
	{%endif%}

	{%for i in paginator.page_range%}
	<a href="{{mket_example_path}}?page={{i}}">
	<div class="goog-inline-block example_page {%if paginator.current_page == i%}example_selected_page{%endif%}">
		{{i}}
	</div> </a>
	{%endfor%}

	{%if paginator.has_next%}
	<div class="goog-inline-block">
		...
	</div>
	<a href="?page={{paginator.next}}">
	<div class="goog-inline-block example_page">
		Next
	</div> </a>
	{%endif%}

</div>
```


### Who do I talk to? ###

* a141890@gmail.com